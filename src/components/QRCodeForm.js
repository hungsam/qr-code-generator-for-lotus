import React, { useState } from "react";
import {
  Box,
  TextField,
  Typography,
  useMediaQuery,
  IconButton,
  ToggleButtonGroup,
  ToggleButton,
  Grid,
  FormControl,
  NativeSelect,
  InputLabel,
} from "@mui/material";
import { Image } from "@mui/icons-material";
import { makeStyles } from "@mui/styles";

import {
  INPUT_ADDRESS_LABEL,
  INPUT_ADDRESS_PLACEHOLDER,
  INPUT_TEXT_LABEL,
  INPUT_TEXT_PLACEHOLDER,
  INPUT_UPLOAD_LOGO_LABEL,
  INPUT_STYLES_LABEL,
  ERR_MSG_INVALID_IMAGE_TYPE,
  TEMPLATE_1,
  TEMPLATE_2,
  TEMPLATE_3,
  TEMPLATE_4,
  DEFAULT_FONT,
  FONT_SERIF,
  FONT_SANS_SERIF,
  FONT_MONOSPACE,
  FONT_HANDWRITING,
} from "../constants";

// Injecting styles with Material UI
const useStyles = makeStyles((theme) => ({
  hideFileInput: {
    display: "none",
  },
}));

// File Upload Button
function UploadImageButton(props) {
  const classes = useStyles();
  const [uploadedFile, setUploadedFile] = useState(null);
  const [errorMsg, setErrroMsg] = useState("");

  // check for correct image file type
  const isValidImageFileType = (file) => {
    // https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types
    const fileTypes = [
      "image/apng",
      "image/bmp",
      "image/gif",
      "image/jpeg",
      "image/pjpeg",
      "image/png",
      "image/svg+xml",
      "image/tiff",
      "image/webp",
      "image/x-icon",
    ];

    return fileTypes.includes(file.type);
  };

  // handle file input change event
  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      if (!isValidImageFileType(file)) {
        setErrroMsg(`${file.name} - ${ERR_MSG_INVALID_IMAGE_TYPE}`);
        return;
      }
      setUploadedFile(file);
      setErrroMsg("");
      props.onChange(file);
    }
  };

  // display the file name or error message
  let desc = "";
  if (errorMsg) {
    desc = errorMsg;
  } else {
    desc = uploadedFile ? uploadedFile.name : "";
  }

  return (
    <>
      <input
        className={classes.hideFileInput}
        type="file"
        accept="image/*"
        name="upload-logo-btn"
        id="upload-logo-btn"
        onChange={handleFileChange}
      />
      <label htmlFor="upload-logo-btn">
        <IconButton color="primary" aria-label="upload logo" component="span">
          <Image />
        </IconButton>
        <Typography component="span" color={errorMsg ? "error" : ""}>
          {desc}
        </Typography>
      </label>
    </>
  );
}

// QRCodeForm Component
function QRCodeForm({
  address,
  text,
  font,
  template,
  logo,
  handleAddressChange,
  handleTemplateChange,
  handleTextChange,
  handleFontChange,
  handleLogoChange,
}) {
  // check the width of the viewport
  const isMD = useMediaQuery((theme) => theme.breakpoints.up("md"));

  // handle template change event
  const onTemplateChange = (event, newTemplate) => {
    if (newTemplate) {
      handleTemplateChange(newTemplate);
    }
  };

  return (
    <Box>
      {/* Address Input */}
      <Box mt={{ sm: 0, md: 1 }}>
        <TextField
          fullWidth
          variant="standard"
          label={INPUT_ADDRESS_LABEL}
          placeholder={INPUT_ADDRESS_PLACEHOLDER}
          name="address"
          value={address}
          onChange={(event) => handleAddressChange(event.target.value)}
        />
      </Box>

      {/* Text Input */}
      <Box mt={{ sm: 0, md: 1 }}>
        <Grid container spacing={1}>
          <Grid item xs={8}>
            <TextField
              fullWidth
              variant="standard"
              label={INPUT_TEXT_LABEL}
              placeholder={INPUT_TEXT_PLACEHOLDER}
              name="text"
              value={text}
              onChange={(event) => handleTextChange(event.target.value)}
            />
          </Grid>
          <Grid item xs={4}>
            <FormControl fullWidth>
              <InputLabel variant="standard" htmlFor="font-selection">
                Font
              </InputLabel>
              <NativeSelect
                defaultValue={font || DEFAULT_FONT}
                inputProps={{
                  name: "font",
                  id: "font-selection",
                  onChange(event) {
                    handleFontChange(event.target.value);
                  },
                }}
              >
                <option value={FONT_SERIF}>{FONT_SERIF}</option>
                <option value={FONT_SANS_SERIF}>{FONT_SANS_SERIF}</option>
                <option value={FONT_MONOSPACE}>{FONT_MONOSPACE}</option>
                <option value={FONT_HANDWRITING}>{FONT_HANDWRITING}</option>
              </NativeSelect>
            </FormControl>
          </Grid>
        </Grid>
      </Box>

      {/* Logo Upload */}
      <Box mt={3}>
        <Typography color="textSecondary" component="span">
          {INPUT_UPLOAD_LOGO_LABEL}
        </Typography>
        <UploadImageButton onChange={handleLogoChange} />
      </Box>

      {/* Templates */}
      <Box mt={3}>
        <Typography color="textSecondary">{INPUT_STYLES_LABEL}</Typography>
        <Box mt={1}>
          <ToggleButtonGroup
            exclusive
            value={template}
            onChange={onTemplateChange}
            aria-label="layout options"
            size={isMD ? "medium" : "small"}
          >
            <ToggleButton
              value={TEMPLATE_1}
              aria-label="qrcode at the top of banner"
            >
              Top
            </ToggleButton>
            <ToggleButton
              value={TEMPLATE_2}
              aria-label="qrcode at the bottom of banner"
            >
              Bottom
            </ToggleButton>
            <ToggleButton
              value={TEMPLATE_3}
              aria-label="qrcode on the right side of banner"
            >
              Right
            </ToggleButton>
            <ToggleButton
              value={TEMPLATE_4}
              aria-label="qrcode on the left side of banner"
            >
              Left
            </ToggleButton>
          </ToggleButtonGroup>
        </Box>
      </Box>
    </Box>
  );
}

export default QRCodeForm;
