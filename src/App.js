import React, { useEffect, useState } from "react";
import WebFont from "webfontloader";
import {
  Box,
  Button,
  Container,
  createTheme,
  ThemeProvider,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import QRCodeForm from "./components/QRCodeForm";
import QRCodeGenerator from "./components/QRCodeGenerator";
import {
  DEFAULT_TEMPLATE,
  DEFAULT_UPLOADED_LOGO,
  DOWNLOAD_QRCODE_LABEL,
  DEFAULT_FONT,
  GENERATED_QR_CODE_GRAPHICS_SVG_ELEMENT,
} from "./constants";

// Align the content of the app
const useStyles = makeStyles((theme) => ({
  center: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "10px",
    marginBottom: "20px",
    minHeight: "40vh",
  },
}));

// Override the primary color of Material UI default theme
const theme = createTheme({
  palette: {
    primary: {
      light: "#7e4ca0",
      main: "#4f2171",
      dark: "#220045",
      contrastText: "#ffffff",
    },
  },
});

// Fonts for QRCode Text
const WebFontConfig = {
  google: {
    families: [
      "Roboto:600",
      "Roboto Slab:600",
      "Roboto Mono:600",
      "Dancing Script:600",
    ],
  },
  fontloading(familyName, fvd) {
    // set the font status to loading
    console.log("loading: ", familyName, fvd);
  },
  fontactive(familyName, fvd) {
    // set the font status to ready
  },
  fontinactive(familyName, fvd) {
    //set the font status to not available
  },
};

// Load Fonts Function
function loadFonts() {
  WebFont.load(WebFontConfig);
}

// App Component
function App() {
  const [address, setAddress] = useState("");
  const [text, setText] = useState("");
  const [font, setFont] = useState(DEFAULT_FONT);
  const [template, setTemplate] = useState(DEFAULT_TEMPLATE);
  const [logoURL, setLogoURL] = useState(DEFAULT_UPLOADED_LOGO);
  const classes = useStyles();

  // load the fonts
  useEffect(() => {
    // use setTimeout to make sure the loading of the font is not blocking
    setTimeout(loadFonts, 0);
  }, []);

  // Event handlers
  const handleAddressChange = (newAddress) => {
    setAddress(newAddress);
  };

  const handleTextChange = (newText) => {
    setText(newText);
  };

  const handleFontChange = (newFont) => {
    setFont(newFont);
  };

  const handleTemplateChange = (newTemplate) => {
    setTemplate(newTemplate);
  };

  const handleLogoChange = (newFile) => {
    if (!newFile) {
      setLogoURL(DEFAULT_UPLOADED_LOGO);
      return;
    }
    const URL = window.URL || window.webkitURL || window;
    setLogoURL(URL.createObjectURL(newFile));
  };

  // Export the QRCODE as Image
  //
  // To convert a svg element to image (PNG, JPG, etc):
  // - we first need to draw that svg element on a canvas
  // - we then export the content of the canvas to image
  // Note:
  // For the svg element to fully rendered on the canvas, all external elements need to be embeded.
  // - Font needs to be embeded
  // - Images need to be embeded
  //
  // The process of converting the QRCOde SVG Template to Image
  // 1. Get a reference to the template svg element
  // 2. Get a reference to the logo image within the svg element
  // 3. Convert the that logo from linking to an external file to embeded data
  // 4. Draw the svg element on a canvas
  // 5. Export the canvas content to image
  const saveAsPNG = () => {
    // get a reference to the svg element
    const svg = document.getElementById(GENERATED_QR_CODE_GRAPHICS_SVG_ELEMENT);
    if (!svg) {
      console.error("Cannot find the generated svg element");
      alert("Something when wrong, cannot find the generated graphics");
      return;
    }

    // get a reference to the logo image within the svg element
    const logoImageEl = svg.getElementsByTagName("image")[0];
    const logoImageUrl = logoImageEl.getAttribute("xlink:href");
    // convert the logo image to Data URL by drawing it on a canvas
    const logoImg = new Image();
    logoImg.src = logoImageUrl;
    logoImg.onload = () => {
      const canvas = document.createElement("canvas");
      canvas.width = logoImg.width;
      canvas.height = logoImg.height;
      const ctx = canvas.getContext("2d");
      ctx.drawImage(logoImg, 0, 0, canvas.width, canvas.height);
      const logoPng = canvas.toDataURL("image/png");
      // replace the external file link with embeded data
      // "xlink:href" is deprecated, use "href" instead
      logoImageEl.setAttribute("xlink:href", logoPng);

      // We cannot draw a svg element directly to a canvas
      // The svg element needs to be drawn as a image
      //
      // Create a Data URL from the svg so we can set it as the src of Image element
      const svgString = new XMLSerializer().serializeToString(svg);
      const svgBlob = new Blob([svgString], {
        type: "image/svg+xml",
      });
      const URL = window.URL || window.webkitURL || window;
      const url = URL.createObjectURL(svgBlob);

      // create an Image element so we can draw it on the canvas
      const img = new Image();
      img.src = url;
      img.onload = () => {
        // draw the image on a canvas
        // convert the canvas to png
        const canvas = document.createElement("canvas");
        canvas.width = svg.getAttribute("width");
        canvas.height = svg.getAttribute("height");
        const ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        const png = canvas.toDataURL("image/png");
        // create a link and download the image
        const a = document.createElement("a");
        a.download = "QRCode.png";
        a.href = png;
        a.click();

        // allow browser to clean up when neccessary
        URL.revokeObjectURL(url);
      };
    };
  };

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="sm">
        <Box className={classes.center}>
          <QRCodeGenerator
            address={address}
            text={text}
            font={font}
            template={template}
            logoURL={logoURL}
          ></QRCodeGenerator>
        </Box>
        <QRCodeForm
          handleAddressChange={handleAddressChange}
          handleTextChange={handleTextChange}
          handleFontChange={handleFontChange}
          handleTemplateChange={handleTemplateChange}
          handleLogoChange={handleLogoChange}
        />
        <Box mt={3} mb={3}>
          <Button
            fullWidth
            color="primary"
            variant="contained"
            onClick={saveAsPNG}
          >
            {DOWNLOAD_QRCODE_LABEL}
          </Button>
        </Box>
      </Container>
    </ThemeProvider>
  );
}

export default App;
