// FORM LABELS
export const INPUT_ADDRESS_LABEL = "Address";
export const INPUT_ADDRESS_PLACEHOLDER = "Your wallet address";
export const INPUT_TEXT_LABEL = "Text";
export const INPUT_TEXT_PLACEHOLDER = "Accepted Here, Give, Tip Us, etc.";
export const INPUT_UPLOAD_LOGO_LABEL = "Upload Your Logo";
export const INPUT_STYLES_LABEL = "Styles";
export const DOWNLOAD_QRCODE_LABEL = "Save As Image";

// Message
export const ERR_MSG_INVALID_IMAGE_TYPE = "Invalid image file type";

// Templates
export const TEMPLATE_1 = "template_1.svg";
export const TEMPLATE_2 = "template_2.svg";
export const TEMPLATE_3 = "template_3.svg";
export const TEMPLATE_4 = "template_4.svg";

// SVG, HTML elements names
export const GENERATED_QR_CODE_GRAPHICS_SVG_ELEMENT =
  "generated_qr_code_graphics_svg_element";

// Fonts
export const FONT_SERIF = "Roboto Slab";
export const FONT_SANS_SERIF = "Roboto";
export const FONT_MONOSPACE = "Roboto Mono";
export const FONT_HANDWRITING = "Dancing Script";

// Default values
export const DEFAULT_UPLOADED_LOGO = "scanme.png";
export const DEFAULT_FONT = FONT_SANS_SERIF;
export const DEFAULT_TEMPLATE = TEMPLATE_1;
